# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0108_auto_20180110_0453'),
    ]

    operations = [
        migrations.CreateModel(
            name='FeaturedContent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('title', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('feature_image', models.ImageField(upload_to='staticfiles/images/featured-content/')),
                ('url', models.URLField(blank=True, default='')),
                ('creation_date', models.DateField(auto_now_add=True)),
                ('modification_date', models.DateTimeField(auto_now=True)),
                ('content', models.ForeignKey(to='core.AudioStudyItem')),
            ],
        ),
        migrations.AlterField(
            model_name='newsletter',
            name='email',
            field=models.EmailField(max_length=70, default=''),
        ),
    ]
