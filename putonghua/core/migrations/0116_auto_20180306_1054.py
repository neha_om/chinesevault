# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0115_auto_20180306_1048'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wordoftheday',
            name='pinyin',
            field=models.CharField(max_length=1024, null=True, default=''),
        ),
    ]
