# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0111_auto_20180222_1130'),
    ]

    operations = [
        migrations.DeleteModel(
            name='PostCheck',
        ),
        migrations.AlterField(
            model_name='wordoftheday',
            name='creation_date',
            field=models.DateField(null=True, default=datetime.datetime(2018, 2, 22, 11, 34, 38, 367087)),
        ),
    ]
