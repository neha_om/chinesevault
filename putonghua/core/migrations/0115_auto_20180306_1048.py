# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0114_auto_20180226_1129'),
    ]

    operations = [
        migrations.AddField(
            model_name='wordoftheday',
            name='pinyin',
            field=models.CharField(max_length=1024, default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='wordoftheday',
            name='creation_date',
            field=models.DateField(unique=True, null=True, default=datetime.date(2018, 3, 6)),
        ),
    ]
