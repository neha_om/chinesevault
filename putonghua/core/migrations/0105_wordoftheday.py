# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0104_subtitled_themes'),
    ]

    operations = [
        migrations.CreateModel(
            name='WordOfTheDay',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('title', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('creation_date', models.DateField(null=True)),
                ('modification_date', models.DateTimeField(auto_now=True)),
                ('description_orig', tinymce.models.HTMLField()),
            ],
        ),
    ]
