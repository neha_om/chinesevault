# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0109_auto_20180222_1005'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='featuredcontent',
            name='url',
        ),
    ]
