# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0113_auto_20180223_0442'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wordoftheday',
            name='creation_date',
            field=models.DateField(unique=True, null=True, default=datetime.date(2018, 2, 26)),
        ),
    ]
