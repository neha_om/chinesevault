# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0110_remove_featuredcontent_url'),
    ]

    operations = [
        migrations.CreateModel(
            name='PostCheck',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('text', models.TextField()),
                ('created_date', models.DateTimeField(default=datetime.datetime(2018, 2, 22, 11, 30, 43, 614896))),
                ('published_date', models.DateTimeField(blank=True, null=True)),
            ],
        ),
        migrations.AlterField(
            model_name='wordoftheday',
            name='creation_date',
            field=models.DateField(null=True, default=datetime.datetime(2018, 2, 22, 11, 30, 43, 613702)),
        ),
    ]
