# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0105_wordoftheday'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='wordoftheday',
            name='description_orig',
        ),
        migrations.AlterField(
            model_name='wordoftheday',
            name='description',
            field=tinymce.models.HTMLField(),
        ),
    ]
